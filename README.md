# Magyar szabad szoftveres dokumentációk

1. __Blender__
   - Kisantal Tibor: Blender – látvány és animáció, 2017. (CC BY-SA 3.0)
1. __E-Közigazgatási Szabad Szoftver Kompetencia Központ__
   - Lakó Viktória: LibreLogo oktatási segédanyag – A teknőcgrafkától a programozáséretségiig, 2013. (CC BY-SA 3.0)
   - Mátó Péter, Rózsár Gábor, Őry Máté, Varga Csaba Sándor, Zahemszky Gábor: 20/80 – Unix és Linux alapismeretek rendszergazdáknak, 2014. (CC BY-SA 3.0)
   - Mátó Péter, Rózsár Gábor, Őry Máté, Varga Csaba Sándor, Zahemszky Gábor: 20/80 – Linux rendszerek alapvető beállításai, üzemeltetése, 2014. (CC BY-SA 3.0)
   - Mátó Péter, Rózsár Gábor, Őry Máté, Varga Csaba Sándor, Zahemszky Gábor: 20/80 – Hálózati szolgáltatások szabad szoftverekkel, 2014. (CC BY-SA 3.0)
   - Torma László: WordPress alapismeretek, 2013. (CC BY-SA 3.0)
   - Varga Péter: Qt strandköny, 2013. (CC BY-SA 3.0)
1. __GIMP__
   - Baráth Gábor: GIMP könyv, 2014. (CC BY 4.0)
1. __Inkscape__
   - Kisantal Tibor: Inkscape – vektorgrafika mindenkinek, 2014. (CC BY-SA 3.0)
1. __LibreOffice__
   - Blahota István: Bevezetés a LibreOffice használatába, 2011. (C)
   - Németh László: Kiadvány­szerkesztés LibreOffice Writer szövegszerkesztővel, 2011. (CC BY-SA-NC-ND 3.0)
   - Writer kalauz, Szövegszerkesztés stílusosan, 2013. (CC BY 3.0)
1. __OpenOffice__
   - Pallay Ferenc: Az OpenOffice.org Calc használata, Táblázatkezelés az alapoktól, 2010. (CC BY-SA 2.5 HU)
1. __openSUSE__
   - openSUSE 11.1 Kézikönyv (PDF), 2009. (GFDL 1.2+)
   - openSUSE 11.2 Kézikönyv (PDF + EPUB), 2010. (GFDL 1.2+)
   - openSUSE 11.3 Kézikönyv (PDF + EPUB), 2010.  (GFDL 1.2+)

